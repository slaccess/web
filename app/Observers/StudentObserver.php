<?php

namespace App\Observers;

use App\Models\Student;
use Webpatser\Uuid\Uuid;

class StudentObserver
{
    public function creating(Student $student) {
        $student->uuid = (string)Uuid::generate();

        $code = null;
        do {
            $code = $this->getAppConfirmCode();
        } while(Student::where('app_activation_code', $code)->count() > 0);

        $student->app_activation_code = (string)$code;
    }

    private function getAppConfirmCode() {
        $length = 8;

        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }
}