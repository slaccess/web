<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentClass;
use Illuminate\Http\Request;

class ClassesController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Classes Controller
    |--------------------------------------------------------------------------
    |
    | ������ ���������� ������������ ������ �������
    | �������� ����� ������� � �����-������
    |
    */

    /**
     * ������� ������ �������,
     * ������������ � ���� ������
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClasses(Request $request) {
        return view('classes/list', [
           'classes' => StudentClass::all(),
        ]);
    }

    /**
     * ������� �����
     * ���������� ������ ������
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd(Request $request) {
        return view('classes/add');
    }

    /**
     * ������������ ������ ��
     * ���������� ������ ������
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAdd(Request $request) {
        $class = new StudentClass();
        $class->name = $request->get('name');
        $class->save();

        return redirect(route('classes'));
    }
}
