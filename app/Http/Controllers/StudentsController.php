<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;
use PHPExcel_Cell;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StudentsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Students Controller
    |--------------------------------------------------------------------------
    |
    | Данный контроллер обеспечивает работу системы
    | управления учениками в админ-панели
    |
    */

    public function getStudents(Request $request) {
        return view('students/list', [
           'students' => Student::all(),
        ]);
    }


    public function getAdd(Request $request) {
        $classesAvailable = StudentClass::all();

        return view('students/add', [
            'classes' => $classesAvailable
        ]);
    }

    public function postAdd(Request $request) {
        $student = new Student();
        $student->first_name = $request->get('first_name');
        $student->last_name = $request->get('last_name');
        $student->class_id = $request->get('class_id');
        $student->save();

        return redirect(route('students'));
    }

    public function getMassAdd(Request $request) {
        $classesAvailable = StudentClass::all();

        return view('students/massAdd', [
            'classes' => $classesAvailable
        ]);
    }

    public function postMassAdd(Request $request) {
        $class_id = $request->get('class_id');

        if(Input::hasFile('file')) {
            $file = Input::file('file');
            $excel = \App::make('excel');

            $excel->load($file->getRealPath(), function($reader) use($class_id) {
                $sheet = $reader->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                for($row = 2; $row <= $highestRow; ++$row) {
                    $val = [];
                    $data = [
                        'first_name' => $sheet->getCellByColumnAndRow(0, $row)->getValue(),
                        'last_name' => $sheet->getCellByColumnAndRow(1, $row)->getValue(),
                    ];

                    $student = new Student();
                    $student->first_name = $data['first_name'];
                    $student->last_name = $data['last_name'];
                    $student->class_id = $class_id;
                    $student->avatar_url = 'http://placehold.it/400x600';
                    $student->save();
                }
            });

            return redirect(route('students'));
        }
    }

    public function getMassAddExample(Request $request) {
        $excel = \App::make('excel');

        return $excel->create('example', function($excel) {
            $excel->setTitle('Шаблон массовой загрузки классов')
                ->setCreator('Ruslan Slinkov')
                ->setCompany('SL Production');

            $excel->sheet('Data', function($sheet) {
                $sheet->fromArray(array(
                    array('Фамилия', 'Имя'),
                ), null, 'A1', false, false);
            });
        })->download('xls');
    }

    public function getShow(Request $request) {

    }
}
