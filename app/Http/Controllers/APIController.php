<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentAccessLogItem;
use Illuminate\Http\Request;

class APIController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | API Controller
    |--------------------------------------------------------------------------
    |
    | ������ ���������� ������������ ������ API �������
    | ����������� ��� �������������� ��������� ���������� �
    | ��������� ���������� �������.
    |
    */

    /**
     * ���������� ����������� ����������
     * � ������ QR ��� �� ������ ���� ���������,
     * ���������� ������������� � ����������-�������.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRegisterApp(Request $request) {
        $appCode = $request->get('code', '');

        $student = Student::where('app_activation_code', $appCode)
            ->where('app_activated', false)
            ->first();

        $student->app_activated = true;
        $student->save();

        return response()->json([
            'userId' => $student->uuid,
            'userName' => sprintf("%s %s", $student->first_name, $student->last_name),
            'avatarUrl' => $student->avatar_url,
            'qrUrl' => route('qr', ['value' => $student->uuid]),
        ]);
    }

    /**
     * ���������� �������� � ���������
     * ������� � ����� ������������ � �����.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postHandleAccess(Request $request) {
        $token = $request->get('token', '');
        $qrValue = $request->get('value', '');

        // ��������� ���������� ������ ����������
        if($token != env('APP_SECRET_TOKEN'))
            return response()->json(['status' => 'error']);

        $student = Student::where('uuid', $qrValue)->first();
        $lastAccessLog = StudentAccessLogItem::where('student_id', $student->id)
            ->orderBy('id', 'desc')->first();

        // ��������� �������� �� ������������� � ���� ������
        if(is_null($student))
            return response()->json(['status' => 'doesntExist']);

        // ���������, �� �������� �� ������� ������� �� ��������
        if(!is_null($lastAccessLog) && date('Y-m-d', strtotime($lastAccessLog->datetime)) == date('Y-m-d'))
            return response()->json(['status' => 'alreadyLogged']);

        $accessLog = new StudentAccessLogItem();
        $accessLog->student_id = $student->id;
        $accessLog->datetime = date("Y-m-d H:i:s");
        $accessLog->save();

        return response()->json(['status' => 'ok']);
    }

    /**
     * ������������ � ������� QR ��� �� ������ ���������� ������:
     * value - �������� �� ����
     * size - ������ ����������� ���� (�� ���������: 300)
     * padding - ������ ������� �� ����� ����������� (�� ���������: 10)
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     */
    public function renderQR(Request $request) {
        $config = [
            'value' => $request->get('value', ''),
            'size' => $request->get('size', 300),
            'padding' => $request->get('padding', 10)
        ];

        $code = new \Endroid\QrCode\QrCode();

        $result = $code->setText($config['value'])
            ->setSize($config['size'])
            ->setPadding($config['padding'])
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->get('jpeg');

        return response($result)
            ->header('Content-Type', 'image/jpeg');
    }
}
