<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;
use PHPExcel_Cell;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PrintController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Print Controller
    |--------------------------------------------------------------------------
    |
    | Данный контроллер обеспечивает работу системы
    | управления учениками в админ-панели
    |
    */

    /**
     * Создает страницу с формой
     * для подготовки кодов приложения на печать.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAppCodes(Request $request) {
        $classesAvailable = StudentClass::all();

        return view('print/appForm', [
            'classes' => $classesAvailable
        ]);
    }

    /**
     * Выводит страницу с
     * подготовленными к печати кодами активации приложения.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postAppCodes(Request $request) {
        $data = $this->getData($request);

        return view('print/appResult', [
            'data' => $data
        ]);
    }

    /**
     * Создает страницу с формой
     * для подготовки пропусков на печать.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPaper(Request $request) {
        $classesAvailable = StudentClass::all();

        return view('print/paperForm', [
            'classes' => $classesAvailable
        ]);
    }

    /**
     * Выводит страницу с
     * подготовленными к печати пропусками.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postPaper(Request $request) {
        $data = $this->getData($request);

        return view('print/paperResult', [
            'data' => $data
        ]);
    }

    /**
     * Получает коллекцию с данными,
     * которые будут переданы в вид.
     * Для своей работы использует
     * параметры $_POST:
     * $_POST['cond'] - тип условия,
     * А также опциональные параметры:
     * $_POST['data_class'] - класс, который выбирается
     * (только если передан cond = 1)
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection|array
     */
    private function getData(Request $request) {
        $condition_type = $request->get('cond');

        $data = [];

        switch($condition_type) {
            case 0:
                $data = Student::where('app_activated', false)->get();
                break;

            case 1:
                $class = $request->get('data_class');
                $data = Student::where('class_id', $class)->get();
                break;
        }

        return $data;
    }
}
