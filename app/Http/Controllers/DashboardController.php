<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentAccessLogItem;
use App\Models\StudentClass;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Dashboard Controller
    |--------------------------------------------------------------------------
    |
    | ������ ���������� ������������ ������ ������� �������� �����-������
    |
    */

    public function getIndex(Request $request) {
        $active = StudentAccessLogItem::whereBetween('datetime',
            [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
            ->count();

        $inactive = Student::count()-$active;

        $late = StudentAccessLogItem::whereBetween('datetime',
            [Carbon::now()->hour(8)->minute(0), Carbon::now()->endOfDay()])
            ->count();

        return view('dashboard', [
            'activeToday' => $active,
            'inactiveToday' => $inactive,
            'lateToday' => $late,
            'chartData' => $this->getChartData(),
        ]);
    }

    private function getChartData() {
        $days = 10;
        $data = [];

        for($i = $days; $i > 0; $i--) {
            $date = new Carbon("-$i days");

            $active = StudentAccessLogItem::whereBetween('datetime',
                [$date->startOfDay(), $date->endOfDay()])
                ->count();

            $late = StudentAccessLogItem::whereBetween('datetime',
                [$date->hour(8)->minute(0), $date->endOfDay()])
                ->count();

            $inactive = Student::count()-$active;

            $data[] = ['date' => $date->format('d.m'), 'active' => $active, 'late' => $late, 'inactive' => $inactive];
        }

        return json_encode($data);
    }
}
