<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use Illuminate\Support\Facades\Auth;

/**
 * �������� ����� �����
 */
Route::group(['middleware' => ['web']], function() {
    Route::get('login', 'Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', 'Auth\AuthController@getLogout');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'DashboardController@getIndex');

        Route::get('classes', ['as' => 'classes', 'uses' => 'ClassesController@getClasses']);
        Route::get('classes/add', ['as' => 'classes/add', 'uses' => 'ClassesController@getAdd']);
        Route::post('classes/add', ['as' => 'classes/add', 'uses' => 'ClassesController@postAdd']);

        Route::get('students', ['as' => 'students', 'uses' => 'StudentsController@getStudents']);
        Route::get('students/add', ['as' => 'students/add', 'uses' => 'StudentsController@getAdd']);
        Route::post('students/add', ['as' => 'students/add', 'uses' => 'StudentsController@postAdd']);
        Route::get('students/massAdd', ['as' => 'students/massAdd', 'uses' => 'StudentsController@getMassAdd']);
        Route::post('students/massAdd', ['as' => 'students/massAdd', 'uses' => 'StudentsController@postMassAdd']);
        Route::get('students/massAddExample', ['as' => 'students/massAddExample', 'uses' => 'StudentsController@getMassAddExample']);
        Route::get('students/show', ['as' => 'students/show', 'uses' => 'StudentsController@getShow']);

        Route::get('print/codes', ['as' => 'print/codes', 'uses' => 'PrintController@getAppCodes']);
        Route::post('print/codes', ['as' => 'print/codes', 'uses' => 'PrintController@postAppCodes']);
        Route::get('print/paper', ['as' => 'print/paper', 'uses' => 'PrintController@getPaper']);
        Route::post('print/paper', ['as' => 'print/paper', 'uses' => 'PrintController@postPaper']);

    });
});

/**
 * ����� API
 */
Route::group(['middleware' => ['api']], function() {
    Route::any('api/registerApplication', 'APIController@postRegisterApp');
    Route::any('api/handleAccess', 'APIController@postHandleAccess');

    Route::any('qr', ['as' => 'qr', 'uses' => 'APIController@renderQR']);
});

