<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAccessLogItem extends Model
{
    protected $table = 'student_access_logs';

    protected $guarded = ['id'];

    protected $dates = ['datetime'];

    public function student() {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
