<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    protected $guarded = ['id'];

    /**
     * ��������� ����� � ������� ������������ ������
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function studentClass() {
        return $this->belongsTo(StudentClass::class, 'class_id');
    }

    /**
     * ���������� ������
     * �� QR ��� ������� �������.
     *
     * @return string
     */
    public function getQrUrl() {
        return route('qr', ['value' => $this->uuid]);
    }
}
