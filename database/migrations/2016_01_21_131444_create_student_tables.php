<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
;
class CreateStudentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('class_id');
            $table->boolean('app_activated');
            $table->string('app_activation_code');
            $table->string('avatar_url');
            $table->timestamps();
        });

        Schema::create('student_access_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->dateTime('datetime');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_classes');
        Schema::drop('students');
        Schema::drop('student_access_logs');
    }
}
