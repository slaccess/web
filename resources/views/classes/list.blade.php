@extends('base')

@section('title')
    Список классов
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Классы
                <small>Управление списком классов</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12 clearfix">
                    <div>
                        <a href="/classes/add" class="btn btn-success pull-right">Добавить класс</a>
                    </div>
                    <br><br><br>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Название</th>
                                    <th>Кол-во учеников</th>
                                </tr>
                                @foreach($classes as $c)
                                    <tr>
                                        <td>{{ $c->id }}</td>
                                        <td>{{ $c->name }}</td>
                                        <td>{{ count($c->students)  }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
@endsection