@extends('base')

@section('title')
    Добавление класса
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавление класса
                <small>Управление списком классов</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Форма</h3>
                        </div>
                        <!-- form start -->
                        <form role="form" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputName">Название класса</label>
                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Название (например: 1А)">
                                </div>
                                {!! csrf_field() !!}
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Информация</h3>
                        </div>
                        <div class="box-body">
                            <p>
                                Воспользуйтесь данной формой, чтобы создать новый класс.<br>
                                Введите <b>Название</b> <i>(Например: 10А)</i> и нажмите кнопку <b>Добавить</b>.
                                <br><br>
                                После того, как вы создадите класс, необходимо загрузить в него учеников.<br>
                                Для этого перейдите на страницу <a href="/students">Ученики</a>.
                            </p>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>

            </div>
        </section>
        <!-- /.content -->
@endsection