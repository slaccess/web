@extends('base')

@section('title')
    Печать
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Печать
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12 clearfix">
                    <div class="box">
                        <div class="table-responsive no-padding">
                            <table class="table table-hover">
                                @foreach($data as $c)
                                    <tr>
                                        <td>
                                            <b>{{ $c->first_name }} {{ $c->last_name }}</b>,
                                            введите этот код при регистрации приложения: <b><i>{{ $c->app_activation_code }}</i></b>.<br>
                                            Скачать приложение для Android можно по ссылке:
                                            <i>http://school62-kras.ru/androidAppBundle.apk</i><br>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body --></div>
                </div>
            </div>
        </section>
        <!-- /.content -->
@endsection

@section('scripts')
    <style>
        @media print {
            .main-footer {
                display: none;
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            window.print();
        });
    </script>
@endsection