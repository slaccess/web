@extends('base')

@section('title')
    Печать бумажных пропусков
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Печать бумажных пропусков
                <small>Управление списком пропусков</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Условия</h3>
                        </div>
                        <!-- form start -->
                        <form role="form" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputName">Режим</label>
                                    <select name="cond" id="condInput" class="form-control">
                                        <option value="0">Неактивированные</option>
                                        <option value="1">По классу</option>
                                    </select>
                                </div>
                                <div class="form-group" id="classInfo" style="display: none;">
                                    <label for="inputName">Класс</label>
                                    <select name="data_class" class="form-control">
                                        @foreach($classes as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                {!! csrf_field() !!}
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Напечатать</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Информация</h3>
                        </div>
                        <div class="box-body">
                            <p>
                                Воспользуйтесь данной формой, чтобы напечатать бумажные пропуски.<br>
                                Введите все необходимые условия и нажмите кнопку <b>Печать</b>.
                                <br><br>
                                Режим <b>"Неактивированные"</b> позволяет вывести на печать пропуски всех пользователей,
                                которые не активировали мобильное приложение.<br>
                                Режим <b>"По классу"</b> позволяет вывести на печать пропуски для всех пользователей
                                конкретного класса.
                                <br><br>
                                В открывшемся диалоговом окне выберите нужный принтер и подтвердите печать.
                            </p>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>

            </div>
        </section>
        <!-- /.content -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#condInput').change(function() {
               $val = $(this).val();

                $('#classInfo').hide();

                if($val == 1)
                    $('#classInfo').show();
            });
        });
    </script>
@endsection