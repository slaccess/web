@extends('base')

@section('title')
    Печать
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Печать
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12 clearfix">
                    <div class="box">
                        @foreach($data as $c)
                            <div style="width: 390px; display: inline-block; vertical-align: top; border: 1px solid #000000; padding: 10px;">
                                <img src="{{ $c->avatar_url }}" style="width: 70px; display: inline-block; vertical-align: top;"/>
                                <div style="display: inline-block; verical-align: top; padding-left: 15px; padding-right: 20px;">
                                    <b style="font-size: 20px;">{{ $c->first_name }}<br>{{ $c->last_name }}</b><br>
                                    <i style="font-size: 18px;">{{ $c->studentClass->name }} класс</i>
                                </div>
                                <img src="{{ $c->getQrUrl() }}" style="float: right;" width="120"/>
                            </div>
                        @endforeach

                        <!-- /.box-body --></div>
                </div>
            </div>
        </section>
        <!-- /.content -->
@endsection

@section('scripts')
    <style>
        @media print {
            .main-footer {
                display: none;
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            window.print();
        });
    </script>
@endsection