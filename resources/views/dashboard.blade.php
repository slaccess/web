@extends('base')

@section('title')
    Главная
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Главная
                <small>Панель управления</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{ $activeToday }}</h3>

                            <p>Сейчас в школе</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-stalker"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{ $inactiveToday }}</h3>

                            <p>Сейчас не в школе</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-close"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-orange">
                        <div class="inner">
                            <h3>{{ $lateToday }}</h3>

                            <p>Сегодня опоздали</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-clock"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->

            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#revenue-chart" data-toggle="tab">Посещаемость</a></li>
                            <li class="pull-left header"><i class="fa fa-inbox"></i> Статистика</li>
                        </ul>
                        <div class="tab-content no-padding">
                            <!-- Morris chart - Sales -->
                            <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                        </div>
                    </div>
                    <!-- /.nav-tabs-custom -->

                </section>
                <!-- /.Left col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
@endsection

@section('scripts')
    <script src="/plugins/morris/morris.min.js"></script>
    <script>
        $(document).ready(function() {
            // Sales chart
            var area = new Morris.Area({
                element: 'revenue-chart',
                resize: true,
                data: {!! $chartData !!},
                xkey: 'date',
                ykeys: ['active', 'inactive', 'late'],
                labels: ['В школе', 'Отсутствуют', 'Опоздали'],
                lineColors: ['#00a65a', '#dd4b39', '#ff851b'],
                hideHover: 'auto'
            });
        });
    </script>
@endsection