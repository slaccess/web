@extends('base')

@section('title')
    Добавление ученика
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавление ученика
                <small>Управление списком учеников</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Форма</h3>
                        </div>
                        <!-- form start -->
                        <form role="form" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputName">Фамилия</label>
                                    <input type="text" name="last_name" class="form-control" id="inputName" placeholder="Фамилия">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Имя</label>
                                    <input type="text" name="first_name" class="form-control" id="inputName" placeholder="Имя">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Класс</label>
                                    <select name="class_id" class="form-control" id="inputName">
                                        @foreach($classes as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {!! csrf_field() !!}
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Информация</h3>
                        </div>
                        <div class="box-body">
                            <p>
                                Воспользуйтесь данной формой, чтобы добавить нового ученика.<br>
                                Введите все необходимые данные и нажмите кнопку <b>Добавить</b>.
                                <br><br>
                                <b>Обратите внимание!</b> Вы можете создать учеников в массовом режиме,
                                для этого перейдите на страницу <a href="/students/massadd">Массовая загрузка</a>.
                            </p>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>

            </div>
        </section>
        <!-- /.content -->
@endsection