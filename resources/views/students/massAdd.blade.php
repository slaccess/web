@extends('base')

@section('title')
    Добавление учеников
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Массовое добавление учеников
                <small>Управление списком учеников</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Загрузка учеников</h3>
                        </div>
                        <!-- form start -->
                        <form role="form" method="post" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputName">Файл .csv</label>
                                    <input type="file" name="file" class="form-control" id="inputName">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Класс</label>
                                    <select name="class_id" class="form-control" id="inputName">
                                        @foreach($classes as $c)
                                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {!! csrf_field() !!}
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Загрузить</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Информация</h3>
                        </div>
                        <div class="box-body">
                            <p>
                                Воспользуйтесь данной формой, чтобы добавить учеников в пакетном режиме.<br>
                                Необходимо загрузить файл .csv со списком учеников, выбрать класс и нажать кнопку <b>Добавить</b>.
                                <br><br>
                                <b>Обратите внимание!</b> Файл необходимо заполнять по шаблону.<br>
                                <b><a href="/students/massAddExample">Нажмите, чтобы скачать шаблон.</a></b>
                            </p>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>

            </div>
        </section>
        <!-- /.content -->
@endsection