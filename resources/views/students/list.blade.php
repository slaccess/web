@extends('base')

@section('title')
    Список учеников
@endsection

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ученики
                <small>Управление списком учеников</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12 clearfix">
                    <div>
                        <a href="/students/add" class="btn btn-success pull-right">Добавить ученика</a>
                    </div>
                    <br><br><br>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Фамилия, имя</th>
                                    <th>Класс</th>
                                    <th>Номер для активации</th>
                                    <th>Опции</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $s)
                                <tr>
                                    <td>{{ $s->id }}</td>
                                    <td>{{ $s->last_name }} {{ $s->first_name }}</td>
                                    <td>{{ $s->studentClass->name }}</td>
                                    <td>
                                        @if($s->app_activated == true)
                                            Активирован
                                        @else
                                            {{ $s->app_activation_code }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('students/show', ['id' => $s->id]) }}" class="btn btn-info">Информация</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
@endsection

@section('scripts')
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#table").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection