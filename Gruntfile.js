// AdminLTE Gruntfile
module.exports = function (grunt) {

  'use strict';

  grunt.initConfig({
    watch: {
      // If any .less file changes in directory "build/less/" run the "less"-task.
      files: ["resources/assets/build/less/*.less", "resources/assets/build/less/skins/*.less", "public/dist/js/app.js"],
      tasks: ["less", "uglify"]
    },
    // "less"-task configuration
    // This task will compile all less files upon saving to create both AdminLTE.css and AdminLTE.min.css
    less: {
      // Development not compressed
      development: {
        options: {
          // Whether to compress or not
          compress: false
        },
        files: {
          // compilation.css  :  source.less
          "dist/css/AdminLTE.css": "resources/assets/build/less/AdminLTE.less",
          //Non minified skin files
          "dist/css/skins/skin-blue.css": "resources/assets/build/less/skins/skin-blue.less",
          "dist/css/skins/skin-black.css": "resources/assets/build/less/skins/skin-black.less",
          "dist/css/skins/skin-yellow.css": "resources/assets/build/less/skins/skin-yellow.less",
          "dist/css/skins/skin-green.css": "resources/assets/build/less/skins/skin-green.less",
          "dist/css/skins/skin-red.css": "resources/assets/build/less/skins/skin-red.less",
          "dist/css/skins/skin-purple.css": "resources/assets/build/less/skins/skin-purple.less",
          "dist/css/skins/skin-blue-light.css": "resources/assets/build/less/skins/skin-blue-light.less",
          "dist/css/skins/skin-black-light.css": "resources/assets/build/less/skins/skin-black-light.less",
          "dist/css/skins/skin-yellow-light.css": "resources/assets/build/less/skins/skin-yellow-light.less",
          "dist/css/skins/skin-green-light.css": "resources/assets/build/less/skins/skin-green-light.less",
          "dist/css/skins/skin-red-light.css": "resources/assets/build/less/skins/skin-red-light.less",
          "dist/css/skins/skin-purple-light.css": "resources/assets/build/less/skins/skin-purple-light.less",
          "dist/css/skins/_all-skins.css": "resources/assets/build/less/skins/_all-skins.less"
        }
      },
      // Production compresses version
      production: {
        options: {
          // Whether to compress or not
          compress: true
        },
        files: {
          // compilation.css  :  source.less
          "dist/css/AdminLTE.min.css": "resources/assets/build/less/AdminLTE.less",
          // Skins minified
          "dist/css/skins/skin-blue.min.css": "resources/assets/build/less/skins/skin-blue.less",
          "dist/css/skins/skin-black.min.css": "resources/assets/build/less/skins/skin-black.less",
          "dist/css/skins/skin-yellow.min.css": "resources/assets/build/less/skins/skin-yellow.less",
          "dist/css/skins/skin-green.min.css": "resources/assets/build/less/skins/skin-green.less",
          "dist/css/skins/skin-red.min.css": "resources/assets/build/less/skins/skin-red.less",
          "dist/css/skins/skin-purple.min.css": "resources/assets/build/less/skins/skin-purple.less",
          "dist/css/skins/skin-blue-light.min.css": "resources/assets/build/less/skins/skin-blue-light.less",
          "dist/css/skins/skin-black-light.min.css": "resources/assets/build/less/skins/skin-black-light.less",
          "dist/css/skins/skin-yellow-light.min.css": "resources/assets/build/less/skins/skin-yellow-light.less",
          "dist/css/skins/skin-green-light.min.css": "resources/assets/build/less/skins/skin-green-light.less",
          "dist/css/skins/skin-red-light.min.css": "resources/assets/build/less/skins/skin-red-light.less",
          "dist/css/skins/skin-purple-light.min.css": "resources/assets/build/less/skins/skin-purple-light.less",
          "public/dist/css/skins/_all-skins.min.css": "resources/assets/build/less/skins/_all-skins.less"
        }
      }
    },
    // Uglify task info. Compress the js files.
    uglify: {
      options: {
        mangle: true,
        preserveComments: 'some'
      },
      my_target: {
        files: {
          'public/dist/js/app.min.js': ['public/dist/js/app.js']
        }
      }
    },
    // Build the documentation files
    includes: {
      build: {
        src: ['*.html'], // Source files
        dest: 'documentation/', // Destination directory
        flatten: true,
        cwd: 'documentation/build',
        options: {
          silent: true,
          includePath: 'documentation/build/include'
        }
      }
    },

    // Optimize images
    image: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'build/img/',
          src: ['**/*.{png,jpg,gif,svg,jpeg}'],
          dest: 'dist/img/'
        }]
      }
    },

    // Validate JS code
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      core: {
        src: 'dist/js/app.js'
      },
      demo: {
        src: 'dist/js/demo.js'
      },
      pages: {
        src: 'dist/js/pages/*.js'
      }
    },

    // Validate CSS files
    csslint: {
      options: {
        csslintrc: 'build/less/.csslintrc'
      },
      dist: [
        'dist/css/AdminLTE.css',
      ]
    },

    // Validate Bootstrap HTML
    bootlint: {
      options: {
        relaxerror: ['W005']
      },
      files: ['pages/**/*.html', '*.html']
    },

    // Delete images in build directory
    // After compressing the images in the build/img dir, there is no need
    // for them
    clean: {
      build: ["build/img/*"]
    }
  });

  // Load all grunt tasks

  // LESS Compiler
  grunt.loadNpmTasks('grunt-contrib-less');
  // Watch File Changes
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Compress JS Files
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // Include Files Within HTML
  grunt.loadNpmTasks('grunt-includes');
  // Optimize images
  grunt.loadNpmTasks('grunt-image');
  // Validate JS code
  grunt.loadNpmTasks('grunt-contrib-jshint');
  // Delete not needed files
  grunt.loadNpmTasks('grunt-contrib-clean');
  // Lint CSS
  grunt.loadNpmTasks('grunt-contrib-csslint');
  // Lint Bootstrap
  grunt.loadNpmTasks('grunt-bootlint');

  // Linting task
  grunt.registerTask('lint', ['jshint', 'csslint', 'bootlint']);

  // The default task (running "grunt" in console) is "watch"
  grunt.registerTask('default', ['watch']);
};
